import std.exception;
import std.conv;

struct Fraction {
  long num;
  long denum;
  this(long num, long denum = 1) {
    enforce(denum != 0, "The denominator cannot be zero");
    this.num = num;
    this.denum = denum;
    if(this.denum < 0) {
      this.num = -this.num;
      this.denum = -this.denum;
    }
  }
  bool opEquals(in Fraction rhs) const {
    double _lhs = this.num / cast(double)this.denum;
    double _rhs = rhs.num / cast(double)rhs.denum;
    return _lhs == _rhs;
  }
  int opCmp(in Fraction rhs) const {
    double _lhs = this.num / cast(double)this.denum;
    double _rhs = rhs.num / cast(double)rhs.denum;
    return _lhs == _rhs ? 0 : _lhs < _rhs ? -1 : 1;
  }
  double opCast(T : double)() const {
    return this.num / cast(double)this.denum;
  }
  Fraction opUnary(string op)() 
    if(op == "-")
  {
    return Fraction(-this.num, this.denum);
  }
  ref Fraction opUnary(string op)() 
    if(op == "--")
  {
    this.num -= this.denum;
    return this;
  }
  ref Fraction opUnary(string op)() 
    if(op == "++")
  {
    this.num += this.denum;
    return this;
  }
  ref Fraction opOpAssign(string op) (in Fraction f) 
    if(op == "+")
  {
    long num = this.num * f.denum + f.num * this.denum;
    this.num = num;
    long denum = this.denum * f.denum;
    this.denum = denum;
    return this;
  }
  ref Fraction opOpAssign(string op) (in Fraction f) 
    if(op == "-")
  {
    long num = this.num * f.denum - f.num * this.denum;
    this.num = num;
    long denum = this.denum * f.denum;
    this.denum = denum;
    return this;
  }
  ref Fraction opOpAssign(string op) (in Fraction f) 
    if(op == "*")
  {
    long num = this.num * f.num;
    this.num = num;
    long denum = this.denum * f.denum;
    this.denum = denum;
    return this;
  }
  ref Fraction opOpAssign(string op) (in Fraction f) 
    if(op == "/")
  {
    long num = this.num * f.denum;
    long denum = this.denum * f.num;
    this.num = num;
    this.denum = denum;
    return this;
  }
  Fraction opBinary(string op) (in Fraction f)
    if(op == "+")
  {
    return Fraction(this.num * f.denum + f.num * this.denum, this.denum * f.denum);
  }
  Fraction opBinary(string op) (in Fraction f)
    if(op == "-")
  {
    return Fraction(this.num * f.denum - f.num * this.denum, this.denum * f.denum);
  }
  Fraction opBinary(string op) (in Fraction f)
    if(op == "*")
  {
    return Fraction(this.num * f.num, this.denum * f.denum);
  }
  Fraction opBinary(string op) (in Fraction f)
    if(op == "/")
  {
    return Fraction(this.num * f.denum, this.denum * f.num);
  }
}
unittest {
  assertThrown(Fraction(42, 0));
  auto a = Fraction(1, 3);
  assert(-a == Fraction(-1, 3));
  ++a;
  assert(a == Fraction(4, 3));
  --a;
  assert(a == Fraction(1, 3));
  a += Fraction(2, 3);
  assert(a == Fraction(1));
  a -= Fraction(2, 3);
  assert(a == Fraction(1, 3));
  a *= Fraction(8);
  assert(a == Fraction(8, 3));
  a /= Fraction(16, 9);
  assertThrown(Fraction(42, 1) / Fraction(0));
  assert(to!double(a) == 1.5);
  assert(a + Fraction(5, 2) == Fraction(4));
  assert(a - Fraction(3, 4) == Fraction(3, 4));
  assert(a * Fraction(10) == Fraction(15));
  assert(a / Fraction(4) == Fraction(3, 8));
  assert(a == Fraction(3, 2));
  assert(Fraction(3, 5) < Fraction(4, 5));
  assert(Fraction(3, 9) < Fraction(3, 8));
  assert(Fraction(1, 1000) > Fraction(1, 10000));
  assert(Fraction(10, 100) < Fraction(1, 2));
  assert(Fraction(-1, 2) < Fraction(0));
  assert(Fraction(1, -2) < Fraction(0));
  assert(Fraction(-1, -2) <= Fraction(1, 2));
  assert(Fraction(1, 2) <= Fraction(-1, -2));
  assert(Fraction(3, 7) <= Fraction(9, 21));
  assert(Fraction(3, 7) >= Fraction(9, 21));
  assert(Fraction(1, 3) == Fraction(20, 60));
  assert(Fraction(-1, 2) >= Fraction(1, -2));
  assert(Fraction(1, 2) >= Fraction(-1, -2));
}
void main() {
}
